<?php 
include 'database.php';



class Controller extends Database{
	function __construct(){
		parent::__construct();

		if($_SERVER['REQUEST_METHOD']=="POST"){
			if(isset($_POST['addPoll'])){
				$this->addPoll();
			}

			if(isset($_POST['action'])){
				if($_POST['action']=='getPoll'){
					$this->getPoll();
				}
				if($_POST['action']=='editActive'){
					$this->editActive();
				}
			}
		}
	}
	function editActive(){
		$id = $_POST['id'];

		$this->db->query("Update harc SET active=0");
		$this->db->query("Update harc SET active=1 where id = $id");
	}
	function getPoll(){
		$data = $this->findAll('harc');

		print json_encode($data);
	}

	function addPoll(){
		$harc = $_POST['harc'];
		$tarberak = $_POST['tarberak'];
		$id = $this->Insert('harc',['harc'=>$harc]);
		
		
		foreach ( $tarberak as  $value) {
			$this->Insert('tarberakner',['tarberak'=>$value,'harc_id'=>$id]);
		}

		header('location:admin.php');
	}
}


$x = new Controller;

 ?>